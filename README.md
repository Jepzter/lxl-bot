# LXL-BOT

#### About
This bot was made for fun, I just wanted to create something. 
The purpose was to build a stable, easy to use bot that sets everything up.


#### Add commands
Adding commands was made to be simple and fast. This is how you do.


```java
package se.jnilsson.commands;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEvent;

public class JokeCommand implements Command {

    @Override
    public <T extends MessageEvent> void execute(T messageEvent) {
        messageEvent.getChannel().sendMessage("What did the baby say to its mother after breastfeeding? - Thanks for the mammaries!");
    }
}
```

And then adding this to the command register like so:
```java
public enum Commands {
    JOKE(new JokeCommand());

    private final Command command;
    Commands(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
```

It's important to know that the enum name you set is the command. In the Discord chat this conforms to !joke

#### Managers

This bot comes with a preset CommandManager that is very simple. What is does is look at the message and map it agains the enums in Commands as seen above. If there is a match it will extract it's command and execute that using the Executor.

You can setup your own service by implementing the Manager interface, and there specify your own way of handling commands. This is the current Manager.


```java
public class CommandManager implements Manager {
    private static final Logger LOG = Logger.getLogger("CommandManager");
    private final Executor executor;

    public CommandManager(Executor executor) {
        this.executor = executor;
    }

    @Override
    public <T extends MessageEvent> void manage(T event) {
        if (event.getMessage().getContent().startsWith("!")) {
            List<String> arguments = getArguments(event.getMessage().getContent());
            try {
                Commands commands = Commands.valueOf(arguments.get(0).toUpperCase());
                executor.setCommand(commands.getCommand());
                executor.execute(event);
            } catch (IllegalArgumentException e) {
                LOG.info(String.format("Could not map user-command: %s to any command in list %s", arguments.get(0), Arrays.toString(Commands.values())));
            }
        }
    }
}
```

#### Executors

Executor is a simple interface with it's only purpose to set the selected command and execute that. This is the executor interface:

````java
public interface Executor {

    void setCommand(Command command);

    <T extends MessageEvent> void execute(T event);
}
````


