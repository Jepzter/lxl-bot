package se.jnilsson.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;

public class ConfigReader {

    public static <T> T read(Class<T> tClass, String path) {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            return mapper.readValue(tClass.getResource(File.separator + path), tClass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
