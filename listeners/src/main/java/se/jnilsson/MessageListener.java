package se.jnilsson;

import se.jnilsson.managers.Service;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class MessageListener {

    private final Service service;

    public MessageListener(Service service) {
        this.service = service;
    }

    @EventSubscriber
    public void handle(MessageReceivedEvent event) {
        service.manage(event);
    }
}
