package se.jnilsson.config;

import static java.util.Objects.requireNonNull;

public class ServerConfiguration {

    private String token;

    public String getToken() {
        return requireNonNull(token);
    }

    public void setToken(String token) {
        this.token = token;
    }
}
