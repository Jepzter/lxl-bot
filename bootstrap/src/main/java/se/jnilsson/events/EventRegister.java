package se.jnilsson.events;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import se.jnilsson.managers.Service;
import sx.blah.discord.api.events.EventDispatcher;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class EventRegister {
    private final static Logger LOG = Logger.getLogger(EventRegister.class.getName());
    private final EventDispatcher dispatcher;
    private final Service service;
    private Set<Method> methods = new HashSet<>();

    public EventRegister(EventDispatcher dispatcher, Service service) {
        this.dispatcher = dispatcher;
        this.service = service;
    }

    public void bind(Class<? extends Annotation> annotation) {
        findClassesWithAnnotation(annotation);
        autoBind(annotation);
    }

    /**
     * Finds all methods in the project with a certain annotation and sets that to methods
     * @param annotation the annotation you want to look for in the project.
     */
    private void findClassesWithAnnotation(Class<? extends Annotation> annotation) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new MethodAnnotationsScanner())
                .setUrls(ClasspathHelper.forPackage("se.jnilsson")));

        methods.addAll(reflections.getMethodsAnnotatedWith(annotation));
    }

    /**
     * Takes the methods set and loops over that getting a single method.
     * it then automatically binds the listener to the dispatcher.
     */
    private void autoBind(Class<? extends Annotation> annotation) {
        methods.forEach(method -> {
            Class clazz = method.getDeclaringClass();
            try {
                if (method.isAnnotationPresent(annotation)) {
                    dispatcher.registerListener(clazz.getDeclaredConstructor(Service.class).newInstance(service));
                    LOG.info(String.format("Successfully autobinded %s", clazz));
                }
                LOG.info(String.format("The annotation %s was not present on the method %s", annotation.getName(), method));
            } catch (InstantiationException | IllegalAccessException e) {
                LOG.severe(String.format("Could not create new instance of class: %s, reason: %s", clazz, e.getMessage()));
            } catch (NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }
}
