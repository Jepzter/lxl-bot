package se.jnilsson;

import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import se.jnilsson.client.Client;
import se.jnilsson.commands.BotCommands;
import se.jnilsson.commands.CommandList;
import se.jnilsson.events.EventRegister;
import se.jnilsson.managers.CommandService;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.api.events.EventSubscriber;

import java.util.Optional;

public class App {
    public static void main( String[] args ) throws IllegalAccessException, InstantiationException {
        EventDispatcher dispatcher = Client.createAndLogin(args[0]).getDispatcher();
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forPackage("*")));

        Optional<Class<?>> first = reflections.getTypesAnnotatedWith(CommandList.class).stream().findFirst();
        BotCommands botCommands = null;
        if(first.isPresent()) {
            botCommands = (BotCommands) first.get().getDeclaringClass().newInstance();
            botCommands.register();
        }

        CommandService commandService = new CommandService(botCommands);
        EventRegister eventRegister = new EventRegister(dispatcher, commandService);

        eventRegister.bind(EventSubscriber.class);
    }
}
