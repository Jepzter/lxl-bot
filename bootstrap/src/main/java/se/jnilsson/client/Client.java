package se.jnilsson.client;

import org.slf4j.LoggerFactory;
import se.jnilsson.config.ConfigReader;
import se.jnilsson.config.ServerConfiguration;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;

public class Client {
    public static final org.slf4j.Logger LOG = LoggerFactory.getLogger("Client");

    public static IDiscordClient create(String conf) {
        ServerConfiguration configuration = ConfigReader.read(ServerConfiguration.class, conf);
        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.withToken(configuration.getToken());
        return clientBuilder.build();
    }

    public static IDiscordClient createAndLogin(String conf) {
        ServerConfiguration configuration = ConfigReader.read(ServerConfiguration.class, conf);
        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.withToken(configuration.getToken());
        return clientBuilder.login();
    }
}
