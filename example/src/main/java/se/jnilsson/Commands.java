package se.jnilsson;

import se.jnilsson.commands.BotCommands;
import se.jnilsson.commands.Command;
import se.jnilsson.commands.CommandList;

@CommandList
public class Commands extends BotCommands {

    @Override
    public void register() {
        addCommand("joke", JokeCommand.create());
    }
}
