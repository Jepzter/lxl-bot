package se.jnilsson.commands;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEvent;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public interface Command<T extends MessageEvent> {
    void execute(T messageEvent);

    String help();
}
