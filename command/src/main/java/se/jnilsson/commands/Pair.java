package se.jnilsson.commands;

public class Pair<L, R> {
    private final L chatCommand;
    private final R command;

    public static Pair create(String chatCommand, Command command) {
        return new Pair<>(chatCommand, command);
    }

    private Pair(L chatCommand, R command) {
        this.chatCommand = chatCommand;
        this.command = command;
    }

    public L getChatCommand() {
        return chatCommand;
    }

    public R getCommand() {
        return command;
    }

    @Override
    public int hashCode() {
        return chatCommand.hashCode() + command.hashCode();
    }
}
