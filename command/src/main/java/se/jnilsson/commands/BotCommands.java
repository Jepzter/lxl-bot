package se.jnilsson.commands;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class BotCommands {
    private Set<Pair<String, Command>> commandSet = new HashSet<>();

    public abstract void register();

    public void addCommand(String chatCommand, Command command) {
        commandSet.add(Pair.create(chatCommand, command));
    }

    public Command getCommand(String chatCommand) throws CommandNotFoundException {
        Optional<Pair<String, Command>> commandPair = commandSet.stream()
                .filter(stringCommandPair -> stringCommandPair.getChatCommand().equals(chatCommand)).findFirst();
        if (commandPair.isPresent()) {
            return commandPair.get().getCommand();
        } else {
            throw new CommandNotFoundException(chatCommand);
        }
    }

    public Set<String> getCommands() {
        return commandSet.stream().map(Pair::getChatCommand).collect(Collectors.toSet());
    }
}
