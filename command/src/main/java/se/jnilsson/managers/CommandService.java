package se.jnilsson.managers;

import se.jnilsson.commands.BotCommands;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEvent;

/**
 * CommandService is the one based on the message is coordinating the given command to it's right instance and executes that.
 */
public class CommandService extends Service {

    public <T extends BotCommands> CommandService(T commands) {
        super(commands);
    }

    @Override
    public void manage(MessageEvent event) {
        super.defaultManager(event);
    }
}
