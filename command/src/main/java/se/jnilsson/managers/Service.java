package se.jnilsson.managers;

import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.jnilsson.commands.BotCommands;
import se.jnilsson.commands.Command;
import se.jnilsson.commands.CommandNotFoundException;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEvent;

/**
 * Managers are used for setting up rules based on a event.
 * The manager should in some way coordinate and execute the right thing.
 */
public abstract class Service<T extends MessageEvent>{
    private final static Logger LOG = LoggerFactory.getLogger(Service.class);

    private final BotCommands commands;

    public <C extends BotCommands> Service(C commands) {
        this.commands = commands;
    }

    public abstract void manage(T event);

    /**
     * Default usage
     *
     * If there is a enum corresponding to the command it will extract it's command class and execute it.
     *
     * @param event the {@link MessageEvent} the comes in
     */
    public void defaultManager(T event) {
        if (event.getMessage().getContent().startsWith("!")) {
            ImmutableList<String> arguments = getArguments(event.getMessage().getContent());
            try {

                Command command = commands.getCommand(arguments.get(0).toUpperCase());
                command.execute(event);
            } catch (CommandNotFoundException e) {
                LOG.error("Could not map user-command: {} to any command in list {}", arguments.get(0), String.join(",", commands.getCommands()));
            }
        }
    }

    private ImmutableList<String> getArguments(String message) {
        message = message.replaceFirst("!", "");
        return ImmutableList.copyOf(message.split(" "));
    }
}
